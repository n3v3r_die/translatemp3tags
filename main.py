import sys
import transliterate
from glob import glob
from mp3_tagger import MP3File
from os.path import exists, join
from transliterate import translit


def TranslateMp3Tags(file_name):
    mp3 = MP3File(file_name)
    mp3.album = translit(mp3.album[0].value, language_code='ru', reversed=False)
    mp3.song = translit(mp3.song[0].value, language_code='ru', reversed=False)
    mp3.artist = translit(mp3.artist[0].value, language_code='ru', reversed=False)
    mp3.save()

def main():
    path = sys.argv[1]
    if not exists(path):
        print(f'Path {path} does not exist.')
        return
    for f in glob(join(path, '*.mp3'), recursive=True):
        print(f'translating tags for {f}')
        TranslateMp3Tags(f)
    print('Done')


if __name__ == "__main__":
    main()
